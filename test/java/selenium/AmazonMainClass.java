package selenium;


import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

class amazon {
    static WebDriver driver;
    JavascriptExecutor je;

    public static void amazonWebApp() {
        try {
            driver = new ChromeDriver();
            System.setProperty("webdriver.chrome.driver", "/home/harshit/IdeaProjects/Amazon/chromedriver");
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get("https://amazon.com");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void userRegistration() {
        try {
            driver.findElement(By.id("nav-link-accountList")).click();
            driver.findElement(By.id("createAccountSubmit")).click();
            driver.findElement(By.cssSelector("#ap_customer_name")).sendKeys("Harshit");
            driver.findElement(By.cssSelector("#ap_email")).sendKeys(""); //email-id or phone number
            driver.findElement(By.cssSelector("#ap_password")).sendKeys(""); //password
            driver.findElement(By.cssSelector("#ap_password_check")).sendKeys(""); //password
            driver.findElement(By.cssSelector("#continue")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void userLogin() {
        try {
            driver.findElement(By.id("nav-link-accountList")).click();
            driver.findElement(By.cssSelector("#ap_email")).sendKeys(""); // phoneNo or Email-id
            driver.findElement(By.cssSelector("#continue")).click();
            driver.findElement(By.cssSelector("#ap_password")).sendKeys(""); // password
            driver.findElement(By.cssSelector("#signInSubmit")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchProductAndFilter() {
        try {
            driver.findElement(By.id("twotabsearchtextbox")).sendKeys("SAMSUNG Galaxy S22 Ultra Smartphone");
            driver.findElement(By.id("nav-search-submit-button")).click();
            driver.findElement(By.xpath("//span[contains(text(),\"SAMSUNG Galaxy S22 Ultra Smartphone, Factory Unlocked Android Cell Phone, 256GB, 8K Camera & Video, Brightest Display, S Pen, Long Battery Life, Fast 4nm Processor, US Version, Phantom Black\")]")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void mouseHoverDropDownList() {
        try {
            Actions ac = new Actions(driver);
            ac.moveToElement(driver.findElement(By.cssSelector("#nav-link-accountList"))).perform();
            driver.findElement(By.xpath("//header/div[@id='navbar']/div[@id='nav-flyout-anchor']/div[@id='nav-flyout-accountList']/div[2]/div[1]/div[2]/a[2]/span[1]")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void addToCartAndCheckOut() {
        try {
            driver.findElement(By.cssSelector("#add-to-cart-button")).click();
            Thread.sleep(4000);
            driver.findElement(By.xpath("//*[@id=\"attach-sidesheet-checkout-button\"]/span/input")).click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }
}


public class AmazonMainClass {
    public static void main(String[] args) {
        amazon webTest = new amazon();
        webTest.amazonWebApp();
        webTest.searchProductAndFilter();
        webTest.addToCartAndCheckOut();

    }
}

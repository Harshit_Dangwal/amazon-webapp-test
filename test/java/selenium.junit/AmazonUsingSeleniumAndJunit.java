package selenium.junit;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AmazonUsingSeleniumAndJunit {
    static WebDriver driver;
    JavascriptExecutor je;

    @BeforeClass
    public static void amazonWebApp() {
        try {
            driver = new ChromeDriver();
            System.setProperty("webdriver.chrome.driver", "/home/harshit/IdeaProjects/Amazon/chromedriver");
            driver.manage().window().maximize();
            driver.manage().deleteAllCookies();
            driver.get("https://amazon.com");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void checkTitle() {
        Assert.assertEquals("Amazon.com. Spend less. Smile more.", driver.getTitle());
    }

    @Test
    public void logoName() {
        Assert.assertTrue(driver.findElement(By.xpath("//a[@aria-label=\"Amazon\"]")).isEnabled());
    }

    @Test
    public void CheckSearchBoxType() {
        Assert.assertEquals("text", driver.findElement(By.id("twotabsearchtextbox")).getDomProperty("type"));
    }

    @AfterClass
    public static void closeBrowser() {
        driver.quit();
    }
}



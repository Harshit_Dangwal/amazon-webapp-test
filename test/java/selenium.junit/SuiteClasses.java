package selenium.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
             AmazonUsingSeleniumAndJunit.class
        }
)

public class SuiteClasses {
}
